#!/usr/bin/env bash

declare -A LABELS
declare -A COMMANDS

COMMANDS["so"]="$text /home/yrro/coding/configs/scripts/skimout.sh"
LABELS["so"]=

#COMMANDS["smb"]="systemctl start home-yrro-.local-bulgan.mount"
#LABELS["smb"]=

COMMANDS["apps"]="/home/yrro/coding/configs/scripts/rofr.sh -r"
LABELS["apps"]=

#COMMANDS["pwr"]="/home/yrro/coding/configs/scripts/rofr.sh -l"
#LABELS["pwr"]=

#4t67 commands
COMMANDS["dnev"]="sh /home/yrro/coding/configs/scripts/dnev.sh"
LABELS["dnev"]=

COMMANDS["bloem"]="$text /home/yrro/4t67/bloem3.md"
LABELS["bloem"]=

COMMANDS["todo"]="$text /home/yrro/4t67/todo.md"
LABELS["todo"]=

COMMANDS["learn"]="$text /home/yrro/4t67/4later.md"
LABELS["learn"]=

COMMANDS["4later"]="$text /home/yrro/4t67/apr-ground/4later.md"
LABELS["4later"]=

# type w and then webaddress, ie w youtube.com
COMMANDS["w"]="$browser\"\${input}\""
LABELS["w"]=

# yandex search
COMMANDS["y"]="webform www.yandex.ru/yandsearch?text="
LABELS["y"]=

COMMANDS["en"]="webform en.wikipedia.org/w/index.php?search="
LABELS["en"]=

COMMANDS["ru"]="webform ru.wikipedia.org/w/index.php?search="
LABELS["ru"]=

COMMANDS["de"]="webform de.wikipedia.org/w/index.php?search="
LABELS["de"]=

COMMANDS["isd"]="$browser digicoll.library.wisc.edu/cgi-bin/IcelOnline/IcelOnline.TEId-idx?type=simple&size=First+100&rgn=lemma&q1=\"\${input}\"&submit=Search"
LABELS["isd"]=

# Applications

COMMANDS["blood"]="nblood"
LABELS["blood"]=

COMMANDS["ifiction"]="gargoyle"
LABELS["ifiction"]=

COMMANDS["only"]="desktopeditors"
LABELS["only"]=

COMMANDS["tele"]="telegram-desktop"
LABELS["tele"]=

COMMANDS["dls"]="urxvt -e fff $HOME/dloads"
LABELS["dls"]=

# former rofr.sh -l

#COMMANDS["lock"]="i3lock-fancy ;"
#LABELS["lock"]=

#COMMANDS["logout"]="session-logout || pkill -15 -t tty\"$XDG_VTNR\" Xorg ;"
#LABELS["logout"]=

COMMANDS["reboot"]="systemctl reboot ;"
LABELS["reboot"]=

COMMANDS["shdown"]="systemctl -i poweroff"
LABELS["shdown"]=

#############################################################
# engine room
#############################################################

##
# Generate menu
##
function print_menu()
{
	for key in ${!LABELS[@]}
	do
		echo "$key    ${LABELS}"
	#	echo "$key    ${LABELS[$key]}"
	# my top version just shows the first field in labels row, not two words side by side
	done
}
##
# Show rofi.
##
function start()
{
	setxkbmap us
	print_menu | rofi -dmenu -p "" -columns 2 -line-padding 4 -lines 2 -width 15 -hide-scrollbar -i
	setxkbmap us,ru
}

# Run it
value="$(start)"

# Split input.
# grab upto first space.
choice=${value%%\ *}
# graph remainder, minus space.
input=${value:$((${#choice}+1))}

# Cancelled? bail out
if test -z ${choice}
then
	exit
fi

# check if choice exists
if test ${COMMANDS[$choice]+isset}
then
	# Execute the choice
#	eval echo "Executing: ${COMMANDS[$choice]}"
	urxvt -e $choice
else
	eval  $choice | rofi
	# prefer my above so I can use this same script to also launch apps like geany or leafpad etc (DK)
	# echo "Unknown command: ${choice}" | rofi -dmenu -p "error"
fi
