#!/bin/sh

pamixer --set-volume 0
sleep 60; pamixer --set-volume 10
sleep 60; pamixer --set-volume 15
sleep 60; pamixer --set-volume 20
sleep 60; pamixer --set-volume 25
sleep 120; pamixer --set-volume 30
sleep 120; pamixer --set-volume 35
sleep 120; pamixer --set-volume 40
sleep 120; pamixer --set-volume 45
sleep 120; pamixer --set-volume 50
