# remove line breaks in a text file

# 0. import necessary modules
import re, sys

# 1. open file
srcpath = sys.argv[1]
retext = open(srcpath)

# 2. convert it to a string
contents = retext.read()

# 3. remove line breaks and multiple spaces
mod = contents.replace("\n", " ")
mod2 = re.sub("\s+", " ", mod)

# 4. save new file
modfilepath = re.sub("\.", "_mod.", srcpath)
retextmod = open(modfilepath, "w")
retextmod.write(mod2)
retextmod.close()
