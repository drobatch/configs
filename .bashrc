# ~/.bashrc

# not running interactively then bail
[[ $- != *i* ]] && return

# shell opts
shopt -s autocd # cd by typing dirname
shopt -s histappend

# prompt
#PS1='\u@\h \W \$ '
export PS1='\[\e[1;95m\][\u \#] \[\e[2;93m\]\W \$\[\e[0m\] '
export HISTCONTROL=ignoreboth:erasedups
export LESS=-R

# aliases
alias lbrm='python ~/coding/configs/scripts/rmlnbreak.py'
alias p='sudo pacman'
alias pa='pikaur'
alias ls='ls --color'
alias dnv='sqlite3 ~/4t67/дневник/dnev-2019.sqlite3'
alias vi='nvim'
alias vim='nvim'
## configuration aliases
alias 3cf='nvim ~/.config/i3/config'
alias ocf='nvim ~/.config/openbox'
alias bcf='nvim ~/.bashrc'
alias pcf='nvim ~/.config/polybar'
alias ccf='nvim ~/.config/compton.conf'
alias acf='nvim ~/.config/alacritty/alacritty.yml'

# added by Anaconda3 2018.12 installer
# >>> conda init >>>
# !! Contents within this block are managed by 'conda init' !!
#__conda_setup="$(CONDA_REPORT_ERRORS=false '/home/yrro/.local/bin/anaconda3/bin/conda' shell.bash hook 2> /dev/null)"
#if [ $? -eq 0 ]; then
#    \eval "$__conda_setup"
#else
#    if [ -f "/home/yrro/.local/bin/anaconda3/etc/profile.d/conda.sh" ]; then
#        . "/home/yrro/.local/bin/anaconda3/etc/profile.d/conda.sh"
#        CONDA_CHANGEPS1=false conda activate base
#    else
#        \export PATH="/home/yrro/.local/bin/anaconda3/bin:$PATH"
#    fi
#fi
#unset __conda_setup
# <<< conda init <<<
